﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class MenuMove : MonoBehaviour {
    private Transform tf;    
    private bool goToEnd = false;
    private bool moving = false;
    private float xStart;
    private float yStart;
    public float xEnd;
    public float yEnd;
    public float speed = 1;
    // Start is called before the first frame update
    void Start() {
        tf = GetComponent<Transform>();
        xStart = tf.localPosition.x;
        yStart = tf.localPosition.y;
    }
    // Update is called once per frame
    void Update() {
        if (moving){
            if (goToEnd) {
                float magnitude = new Vector2((xEnd - xStart), (yEnd - yStart)).magnitude;
                Vector2 distance = new Vector2((xEnd - tf.localPosition.x), (yEnd - tf.localPosition.y));
                distance.Normalize();
                distance *= magnitude;
                Debug.Log(distance);
                tf.position = new Vector2( (tf.position.x + (distance.x * Time.deltaTime)/(10 / speed)), (tf.position.y + (distance.y * Time.deltaTime)/(10 / speed)));

                if(Mathf.Abs(xEnd - tf.localPosition.x) < 0.75 && Mathf.Abs(yEnd - tf.localPosition.y) < 0.75){
                    tf.localPosition = new Vector2(xEnd, yEnd);
                }
                Debug.Log("Y: " + yEnd + ", " + tf.localPosition.y);
                Debug.Log("X: " + xEnd + ", " + tf.localPosition.x);
                if(tf.localPosition.x == xEnd && tf.localPosition.y == yEnd){
                    moving = false;
                }
            }
            else {

            }
        }
    }
    public void clicked() {
        moving = true;
        goToEnd = !goToEnd;
    }
}
