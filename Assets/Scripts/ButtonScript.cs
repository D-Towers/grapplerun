﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class ButtonScript : MonoBehaviour
{
    public GameObject Star;
    public TextAsset ta;
    public int levelNum;
    public bool complete = false;
    public bool playable = false;
    public int scene;
    bool once = false;
    // Start is called before the first frame update
    void Start()
    {
        string sr = PlayerPrefs.GetString("levelsComplete");
        Debug.Log(sr);
        char[] ca = sr.ToCharArray();
        for (int i = 0; i < ca.Length; i++){
            if(int.Parse(ca[i].ToString()) == levelNum - 1){
                playable = true;
            }
            if(int.Parse(ca[i].ToString()) == levelNum){
                complete = true;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (playable){
            UnityEngine.UI.Button btn = this.GetComponent<UnityEngine.UI.Button>();
            btn.interactable = true;
            btn.onClick.AddListener(clicked);
        }
        if (complete)
            Star.GetComponent<UnityEngine.UI.Image>().enabled = true;
        
    }
    void clicked(){
        if (!once) {
            once = true;
            PlayerPrefs.SetInt("sceneTo", scene);
            UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(2);
        }
    }
}
