﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Class controls behaviour of Lazer
public class Lazer : MonoBehaviour
{
    public float speed;
    // Start is called before the first frame update
    void Start() {
        
    }

    // Update is called once per frame
    void Update() {   
        //Lazer moves slowly to the right to make players do something, but will stop before cave exit
        if(gameObject.transform.position.x < 26)
            gameObject.transform.position += new Vector3(speed * (Time.deltaTime * 50), 0, 0);
    }
}
