﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Class defines several useful Debug utilities
public class debug : MonoBehaviour
{
    GameObject player;
    GameObject text;
    string DebugString;
    bool flightmode = false;
    bool godmode = false;
    public Vector3 pos1 = new Vector3(0, 0, 2);
    public Vector3 pos2 = new Vector3(0, 0, 2);
    

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Player");
        text = GameObject.Find("Debug");
        text.transform.position = new Vector2(300, Screen.height - 40);
        
    }

    // Update is called once per frame
    void Update()
    {
        //Teleoprts player outside the cave
        if (Input.GetKeyDown(KeyCode.Alpha1)){
            player.transform.position = pos1;
        }

        //Teleports the Player to just before the end of the level
        if (Input.GetKeyDown(KeyCode.Alpha2)){
            player.transform.position = pos2;
        }

        //Enables God mode, Player is set to a layer that interacts with the ground but is not effected by enemies
        if (Input.GetKeyDown(KeyCode.F1)){
            if (player.layer == 12)
                player.layer = 15;
            else
                player.layer = 12;
            godmode = !godmode;
        }

        //Enables Flightmode, allows player to move freely through the map without colliding with anything
        if (Input.GetKeyDown(KeyCode.F2)){
            if(!flightmode){
                player.GetComponent<Rigidbody2D>().gravityScale = 0;
                player.GetComponent<Rigidbody2D>().isKinematic = true;
                player.GetComponent<PlayerController>().enabled = false;
                flightmode = true;
            }
            else {
                player.GetComponent<Rigidbody2D>().gravityScale = 3;
                player.GetComponent<Rigidbody2D>().isKinematic = false;
                player.GetComponent<PlayerController>().enabled = true;
                flightmode = false;
            }
        }

        //Controls for flight mode
        if(flightmode){
            if (Input.GetKey(KeyCode.W)){
                player.transform.position += new Vector3(0, 0.1f, 0);
            }
            if (Input.GetKey(KeyCode.A)){
                player.transform.position += new Vector3(-0.1f, 0, 0);
            }
            if (Input.GetKey(KeyCode.S)){
                player.transform.position += new Vector3(0, -0.1f, 0);
            }
            if (Input.GetKey(KeyCode.D)){
                player.transform.position += new Vector3(0.1f, 0, 0);
            }
        }
        
        //When using debug modes, they will be displayed on the screen
        text.GetComponent<UnityEngine.UI.Text>().text = (godmode? "Godmode " : "") + (flightmode ? "Flightmode" : "");
    }
}
