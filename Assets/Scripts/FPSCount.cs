﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Class that tracks the FPS and displays it on the screenw
public class FPSCount : MonoBehaviour
{
    UnityEngine.UI.Text text;
    // Start is called before the first frame update
    void Start()
    {
        text = gameObject.GetComponent<UnityEngine.UI.Text>();
        text.transform.position = new Vector2(100, Screen.height - 40);
    }

    // Update is called once per frame
    void Update()
    {
        //Frame rate is roughly 1/the time taken for the last frame, caps at screen refresh rate
        text.text = "FPS: " + (int) (1.0f/Time.deltaTime);
    }
}
