﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class hook : MonoBehaviour
{
    private Rigidbody2D rb;
    public GameObject projectile;
    public float speed = 5;
    public bool fired = false;
    private GameObject bullet;
    private DistanceJoint2D dj;
    private LineRenderer lr;
    public AudioClip hookSFX;

    AudioSource sfx;
    void Start() {
        sfx = gameObject.GetComponent<AudioSource>();
        rb = GetComponent<Rigidbody2D>();
        bullet = null;
        dj = GetComponent<DistanceJoint2D>();
        lr = GetComponent<LineRenderer>();
    }

    void FixedUpdate() {
        //If the player clicks the mouse button and is not dead
        if (Input.GetMouseButton(0) && !gameObject.GetComponent<PlayerController>().dead) {
            if (!fired) {
                sfx.clip = hookSFX; 
                sfx.Play();
                Vector3 worldMousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

                Vector2 direction = (Vector2)((worldMousePos - transform.position));
                direction.Normalize();

                // Creates the bullet with a velocity towards the cursor
                bullet = (GameObject)Instantiate(projectile, transform.position + (Vector3)(direction * 0.5f), Quaternion.identity);
    
                // Adds velocity to the bullet
                bullet.GetComponent<Rigidbody2D>().velocity = direction * speed;     
                bullet.GetComponent<Rigidbody2D>().MoveRotation(Mathf.Atan2(direction.x, -direction.y) * Mathf.Rad2Deg - 90);
                fired = true;
            }
            
        }

        //If the player has let go of the mouse destroy the objects
        else if (fired) {
            if (bullet.GetComponent<BulletScipt>().caught != null){
                bullet.GetComponent<BulletScipt>().caught.layer = 10;
                bullet.GetComponent<BulletScipt>().caught = null;
            }
            clean();
            
        }

        //If the hook has collided with something
        if (bullet != null && ((bullet.GetComponent<Rigidbody2D>().velocity == new Vector2(0, 0)) || bullet.GetComponent<BulletScipt>().enCaught))
        {
            //enable the line renderer and distance joint
            if (dj.enabled == false) {
                dj.enabled = true;
                lr.enabled = true;
            }

            // If it hasn't collided with an enemy attatch anchor to collision
            if(bullet.GetComponent<BulletScipt>().enCaught == false){
                dj.connectedAnchor = bullet.GetComponent<Rigidbody2D>().GetRelativePoint(new Vector2(-0.16f, 0));
            }
            
            // If it collided witht the enemy set attatch anchor to enemy
            else if (bullet.GetComponent<BulletScipt>().caught != null) {
                dj.connectedBody = bullet.GetComponent<BulletScipt>().caught.GetComponent<Rigidbody2D>();
            }
            
            //If player holds shift, either bring player to anchor, or bring enemy to player           
            if (Input.GetKey(KeyCode.LeftShift))
            {
                if(bullet.GetComponent<BulletScipt>().enCaught == false){
                    rb.transform.position = Vector2.MoveTowards(rb.transform.position, bullet.GetComponent<Rigidbody2D>().position, 0.25f);
                }
                else {
                    bullet.GetComponent<BulletScipt>().caught.GetComponent<Rigidbody2D>().transform.position = Vector2.MoveTowards(bullet.GetComponent<BulletScipt>().caught.GetComponent<Rigidbody2D>().transform.position, rb.position, 0.25f);
                }
            }

            if (Input.GetKey(KeyCode.LeftControl)) {
                if(bullet.GetComponent<BulletScipt>().enCaught == false){
                    rb.transform.position = Vector2.MoveTowards(rb.transform.position, bullet.GetComponent<Rigidbody2D>().position, -0.25f);
                }
            }

        }
        // disable the line renderer and distance joint
        else
        {
            dj.enabled = false;
            lr.enabled = false;
        }
    }

    public void clean(){
        if (bullet != null)
            Destroy(bullet);
        fired = false;
        lr.enabled = false;
    }
}
