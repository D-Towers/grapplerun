﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOver : MonoBehaviour
{
    SpriteRenderer sr;
    GameObject player;
    GameObject cam;
    PlayerController ps;
    DistanceJoint2D dj;
    LineRenderer lr;
    float time = 0;
    float camSize;
    bool once = false;
    public int scene;
    // Start is called before the first frame update
    void Start()
    {
        cam = GameObject.Find("Main Camera");
        camSize = cam.GetComponent<Camera>().orthographicSize;
        sr = GetComponent<SpriteRenderer>();
        player = GameObject.Find("Player");
        ps = player.GetComponent<PlayerController>();
        lr = gameObject.GetComponent<LineRenderer>();
        gameObject.transform.position = new Vector3((cam.transform.position.x - (camSize * 2.134f)) + 2.56f , cam.transform.position.y + camSize - 2.56f, -9);
    }

    // Update is called once per frame
    void Update()
    {
        //If the player dies, run the game over feature
        if (ps.dead)
        {
            time += Time.deltaTime;
            //Render a rope from the top of the screen to the Game Over object
            if (lr.enabled == false)
                lr.enabled = true;
            Vector3[] pos = new Vector3[2];
            pos[0] = cam.transform.position;
            pos[1] = gameObject.transform.position;
            pos[0].z = 2;
            pos[0].y += camSize;
            pos[1].z = 2;
            lr.SetPositions(pos);

            //Enable the sprite to render, and apply gravity
            sr.enabled = true;
            sr.GetComponent<Rigidbody2D>().gravityScale = 4;
            
            //enable a distance joint and allow object to swing on screen
            if (dj == null) {
                Transform trans = cam.GetComponent<Transform>();
                dj = gameObject.AddComponent<DistanceJoint2D>();
                dj.connectedAnchor = new Vector2(trans.position.x, trans.position.y + camSize);
            }
            if (time > 3 && !once){
                once = true;
                UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(scene);
            }
        }
    }
}
