﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadingScript : MonoBehaviour
{
    public UnityEngine.UI.Text message;
    float time = 0;
    float loadTime = 0;
    public float waitTime = 1;
    public string msgText;
    string dots;
    bool once = false;
    int scene;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update() {
        time += Time.deltaTime;
        loadTime += Time.deltaTime;
        if ( loadTime > waitTime) {
            loadTime = 0;
            dots += ".";
            if (dots.Length > 3){
                dots = "";
            }
            message.text = msgText + dots;
        }
        if(time > 3 && !once) {
            once = true;
            UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(PlayerPrefs.GetInt("sceneTo"));
        }
    }
}
