﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Class for controlling the background movement
public class Background : MonoBehaviour
{
    GameObject player;
    Rigidbody2D prd;
    Transform trans;
    public int divisor;
    GameObject camPos;
    float camSize;
    float movement = 15.36f;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Player");
        prd = player.GetComponent<Rigidbody2D>();
        trans = gameObject.transform;     
        camPos = GameObject.Find("Main Camera");
        //Camsize is multilpied by 2.134 as this is off screen for all tested apect ratios
        camSize = camPos.GetComponent<Camera>().orthographicSize * 2.134f; 

    }

    // Update is called once per frame
    void Update()
    {
        //If Player is Dead stop moving the Background
        if(!player.GetComponent<PlayerController>().dead){
            //Move background in oppisite direction to player, adjusted on predetermined "distance" in background
            trans.position = new Vector3(trans.position.x - (prd.velocity.x/divisor), trans.position.y, trans.position.z);
            trans.position = new Vector3(trans.position.x, trans.position.y - (prd.velocity.y/(divisor * 2)), trans.position.z);

            //If Background Object has gone offscreen move it to the other side to make it seem like the background is infinite
            if (trans.position.x < camPos.transform.position.x - (camSize) - movement){
                trans.position = new Vector2(trans.position.x + ((camSize) * 4) + movement, trans.position.y);
            }
            else if (trans.position.x > camPos.transform.position.x + (camSize) + movement){
                trans.position = new Vector2(trans.position.x - ((camSize) * 4) - movement, trans.position.y);
            }
        }
    }
}
