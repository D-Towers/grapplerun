﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Class controls movement of camera to ensure player is within boundaries
public class CameraScript : MonoBehaviour
{
    GameObject player;
    Transform ppos;
    Transform cpos;
    float camSize;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Player");
        ppos = player.transform;
        cpos = gameObject.transform;
        camSize = gameObject.GetComponent<Camera>().orthographicSize -1;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        //Prevent the camera from following player if the player is dead.
        if(!player.GetComponent<PlayerController>().dead){
            Vector3 newPos = cpos.position;

            //If player is nearing edge of camera, move the camera to prevent Player going to edge
            if (Mathf.Abs(ppos.position.x - cpos.position.x) > camSize)
            {
                newPos.x  += (Mathf.Abs(ppos.position.x - cpos.position.x) - camSize) * ((ppos.position.x < cpos.position.x) ? -1 : 1);
            }
            if (Mathf.Abs(ppos.position.y - cpos.position.y) > camSize - 2)
            {
                newPos.y += (Mathf.Abs(ppos.position.y - cpos.position.y) - camSize + 2) * ((ppos.position.y < cpos.position.y) ? -1 : 1);
            }

            //If the camera isn't centred move the camera to put player close to the centre of the camera
            if (Mathf.Abs(cpos.position.y - ppos.position.y) >=0.2f){
                newPos.y += Mathf.Abs(cpos.position.y - ppos.position.y) * (ppos.position.y < cpos.position.y ? -0.02f : +0.02f);
            }

            if (Mathf.Abs(cpos.position.x - ppos.position.x) >= 1.5f){
                newPos.x += Mathf.Abs(cpos.position.x - ppos.position.x) * (ppos.position.x < cpos.position.x ? -0.02f : +0.02f);
            }
            cpos.position = newPos;
        }
    }
}
