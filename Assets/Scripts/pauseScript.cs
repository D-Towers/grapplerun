﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pauseScript : MonoBehaviour
{
    public UnityEngine.UI.Button resume;
     UnityEngine.UI.Text resTxt;
    public UnityEngine.UI.Button mm;
    UnityEngine.UI.Text mmTxt;
    AudioSource ads;
    public UnityEngine.UI.Image img;
    bool once = false;
    public bool paused = false;
    // Start is called before the first frame update
    void Start() {
        ads = GameObject.Find("Audio Source").GetComponent<AudioSource>();
        resTxt = resume.GetComponentInChildren<UnityEngine.UI.Text>();
        mmTxt = mm.GetComponentInChildren<UnityEngine.UI.Text>();

        resume.onClick.AddListener(resumeClick);
        mm.onClick.AddListener(mmClick);

        img.enabled = false;
        
        mmTxt.enabled = false;
        mm.image.enabled = false;
        
        resTxt.enabled = false;
        resume.image.enabled = false;
    }

    // Update is called once per frame
    void Update() {
        if (Input.GetKeyDown(KeyCode.Escape)){
            setTimeScale(paused);
            paused = !paused;
            
            resTxt.enabled = paused;
            resume.image.enabled = paused;
           
            mmTxt.enabled = paused;
            mm.image.enabled = paused;

            img.enabled = paused;
        }
    }
    void resumeClick() {
        setTimeScale(true);
        resume.image.enabled = false;
        resTxt.enabled = false;
        mm.image.enabled = false;
        mmTxt.enabled = false;
        img.enabled = false;
    }

    void mmClick() {
        if (!once){
            once = true;
            PlayerPrefs.SetInt("sceneTo", 0);
            Time.timeScale = 1;
            UnityEngine.SceneManagement.SceneManager.LoadScene(2);
        }
    }

    void setTimeScale(bool isPaused){
        if(!isPaused) {
            Time.timeScale = 0;
            ads.Pause();
        }
        else {
            Time.timeScale = 1;
            ads.Play();
        }
    }
}
