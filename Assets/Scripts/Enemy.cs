﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Class controls enemy behaviour
public class Enemy : MonoBehaviour
{
    // Start is called before the first frame update
    GameObject player;
    AudioSource sfx;
    ParticleSystem ps;
    public float jumpHeight = 250;
    public float walkSpeed = 160;
    public int grounded = 0;
    public bool facingRight = true;
    public float jumpTime = 0;
    public bool looked = false;
    int multiplier = 1;
    bool dead = false;
    float time = 0; 
    void Start()
    {
        player = GameObject.Find("Player");
        sfx = gameObject.GetComponent<AudioSource>();
        ps = gameObject.GetComponent<ParticleSystem>();
        ps.Stop();
    }

    // Update is called once per frame
    void Update()
    {
        if (dead){
            time += Time.deltaTime;
            ps.Emit(5);
            sfx.Play();
            gameObject.GetComponent<SpriteRenderer>().enabled = false;
            if (time > 0.2) {
                Destroy(gameObject);
            }
        }
        else {
            //Finds where it and the player are in world
            Vector3 ppos = player.transform.position;
            Vector3 pos = gameObject.transform.position;

            //If Enemy is on ground increment jump timer
            if (grounded > 0)
                jumpTime += Time.deltaTime;

            //After being still and are able to jump, enemy will turn ot the direction about to jump in
            if (jumpTime > 0.75 && looked == false){
                looked = true;
                //Flips the direction the Slime is looking if player is within range
                if (Vector2.Distance(ppos, pos) < 5){
                    if (ppos.x > pos.x){
                        multiplier = 1;
                        if (!facingRight)
                            reverseImage();
                    }
                    else if (ppos.x < pos.x){
                        multiplier = -1;
                        if(facingRight)
                            reverseImage();
                    }
                }

                //Otherwise flip the image
                else {
                    multiplier *= -1;
                    reverseImage();
                }
            }

            //Jump in direction
            if (jumpTime > 1.5) {
                gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(walkSpeed * multiplier * (Vector2.Distance(ppos, pos) < 5 ? 1 :  0.65f), (grounded > 0 ? jumpHeight * (Vector2.Distance(ppos, pos) < 5 ? 1 :  0.65f): 0)));
                jumpTime = 0;
                
                looked = false;
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        //resets ability to jump when landing on the ground
        if (collision.gameObject.layer == 8)
        {
            gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(0,0);
            grounded++;
            if (grounded > 1 )
                grounded = 1;
        }
        //If the enemy is caught by players hook, set the layer to 14
        else if(collision.gameObject.layer == 9){
            gameObject.layer = 14;
        }
        //If the enemy collides witht the player, kill the player unless the player is above the slime
        else if(collision.gameObject.name == "Player"){
            if(player.transform.position.y - 0.36 > gameObject.transform.position.y + 0.1 || gameObject.layer == 14){
                dead = true;
                hook hk = player.GetComponent<hook>();
                hk.clean();
                player.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 300));
            }
            else {
                player.GetComponent<PlayerController>().dead = true;
                hook hk = player.GetComponent<hook>();
                hk.clean();
                player.GetComponent<PlayerController>().deathNoise();
                Destroy(player);
            }
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        // prevent jumping in air
        if (collision.gameObject.layer == 8)
        {
            sfx.Play();
            grounded--;
            if (grounded < 0)
                grounded = 0;
        }
    }

    private void reverseImage(){
        facingRight = !facingRight;

        // Get and store the local scale of the RigidBody2D
        Vector2 theScale = gameObject.GetComponent<Rigidbody2D>().transform.localScale;

        // Flip it around the other way
        theScale.x *= -1;
        gameObject.GetComponent<Rigidbody2D>().transform.localScale = theScale;
    }
}
