﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Emitter : MonoBehaviour {
    float time = 2;
    public GameObject spike;
    // Start is called before the first frame update
    void Start() {
        
    }

    // Update is called once per frame
    void Update() {
        time += Time.deltaTime;
        if (time > 2) {
            time = 0;
            Instantiate(spike, transform.position, Quaternion.identity);
        }
    }
}
