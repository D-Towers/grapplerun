﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Class for controlling clouds
public class cloud : MonoBehaviour
{
    GameObject cam;
    Transform camPos;
    float camSize;
    Transform pos;
    float cloudSpeed;
    Vector3 oldCamPos;
    private float startHeight;
    // Start is called before the first frame update
    void Start()
    {
        pos = gameObject.transform;
        cam = GameObject.Find("Main Camera");
        camPos = cam.transform;
        //Camsize is multilpied by 2.134 as this is off screen for all tested apect ratios
        camSize = cam.GetComponent<Camera>().orthographicSize * 2.134f;

        //clouds start at random height
        cloudSpeed = Random.Range(0, 1);
        cloudSpeed += 0.24f;
        startHeight = pos.transform.position.y;
    }

    // Update is called once per frame
    void Update()
    {
        //Clouds move to the left
        Vector3 newPos = new Vector3(pos.position.x - (cloudSpeed * (Time.deltaTime * 50) /10), pos.position.y, pos.position.z);

        //When clouds go off screen move them to the other side at a random height to give the effect of infinite clouds.
        if (newPos.x < camPos.position.x - (camSize * 2.134f/2) - 3f){
            newPos.x += (camSize * 2.134f) + 6f;
            float height = Random.Range(-2, 3);
            newPos.y = startHeight + height;
        }

        //If camera is moving to the left, clouds keep up to prevent them being left too far on the right
        if( oldCamPos != null && oldCamPos.x > camPos.position.x){
            newPos += new Vector3((camPos.position.x - oldCamPos.x) + (cloudSpeed-0.25f)/10 ,0 ,0);
        }
        
        pos.position = newPos;
        oldCamPos = camPos.position;
    }
}
