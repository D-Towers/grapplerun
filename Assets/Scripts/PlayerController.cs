﻿using UnityEngine;
using System.Collections;

//Class for controlling player behaviour
public class PlayerController : MonoBehaviour {    
    float maxSpeed = 11;
    //Character starts facing Right
    private bool facingRight = true;
    public int jumpHeight = 300;
    private Rigidbody2D rb;
    private Animator anim;
    private int time;
    bool move = false;
    public int grounded = 0;
    public bool dead = false;
    AudioSource sfx;
    public AudioClip jumpSFX;
    public AudioClip DeathSFX;

    void Start()
    {
        sfx = gameObject.GetComponent<AudioSource>();
        //Application.targetFrameRate = 60;
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
    }
    
    void Update()
    {
         Application.targetFrameRate = 60;

        if (!dead) {
            anim.SetFloat("speed", Mathf.Abs(rb.velocity.x));
            // Allow player to move left and right using the A and D keys
            if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.D))
            {
                //Use h as it will build up to 1 allow a short build up in speed
                float h = Input.GetAxis("Horizontal");
                if (Mathf.Abs(rb.velocity.x) < maxSpeed - 0.5 || Mathf.Abs(h + rb.velocity.x) < Mathf.Abs(rb.velocity.x)){
                    rb.AddForce(new Vector2(h * maxSpeed, 0));
                    
                    move = true;
               }
                
                //Turn character to face correct way
                if (h > 0 && !facingRight)
                    reverseImage();
                else if (h < 0 && facingRight)
                    reverseImage();
            }
            // If player is not holding down A or D, slow down the Player character
            else if (rb.velocity.magnitude == 0) {
               move = false;
            }
            
            //Tell the animator to use the movement animation
            anim.SetBool("Moving", move);

            //Jump when space is pressed
            if (Input.GetKeyDown(KeyCode.Space) && grounded > 0)
            {
                sfx.clip = jumpSFX;
                sfx.Play();
                rb.AddForce(new Vector2(0, jumpHeight));                
            }
        }
    }

    //Flip the image and swap the boolean 
    void reverseImage() {
        facingRight = !facingRight;
        Vector2 theScale = rb.transform.localScale;
        theScale.x *= -1;
        rb.transform.localScale = theScale;
    }

    private void OnCollisionEnter2D(Collision2D collision) {
        //When landing after jumping reset number of jumps
        if (collision.gameObject.layer == 8) {
            grounded++;
            if (grounded > 1 )
                grounded = 1;
        }

        //If colliding with an active enemy, kill player
        if (collision.gameObject.layer == 13) {
            gameObject.GetComponent<CapsuleCollider2D>().enabled = false;
            dead = true;
            deathNoise();
        }
    }

    private void OnCollisionExit2D(Collision2D collision) {
        //Take away jumps when leaving surface
        if (collision.gameObject.layer == 8) {
            grounded--;
            if (grounded < 0)
                grounded = 0;
        }
    }

    public void deathNoise(){
        sfx.clip = DeathSFX;
        sfx.Play();
    }
}