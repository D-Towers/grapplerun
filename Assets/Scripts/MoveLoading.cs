﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveLoading : MonoBehaviour
{
    public float speed;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.localPosition = new Vector2(transform.localPosition.x - (Time.deltaTime * speed), transform.localPosition.y + (Time.deltaTime * speed));
        if (transform.localPosition.x < -1060.66f) {
            transform.localPosition = new Vector2(transform.localPosition.x + 2121.32f,transform.localPosition.y - 2121.32f);
        }
    }
}
