﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Class for controlling behaviour of the Hook/Projectile when fired
public class BulletScipt : MonoBehaviour
{
    public bool collided = false;
    public bool enCaught = false;
    Rigidbody2D rb;
    public float x;
    public float y;
    private GameObject player;
    public GameObject caught;
    private LineRenderer lr;
    public AudioClip objhit;
    public AudioClip enehit;
    AudioSource sfx;
    void Start() {
        rb = GetComponent<Rigidbody2D>();
        player = GameObject.Find("Player");
        lr = player.GetComponent<LineRenderer>();
        sfx = gameObject.GetComponent<AudioSource>();
    }

    void Update() {
        //Renders a line (rope) between the player and the Bullet (Projectile)
        if (lr.enabled == false)
            lr.enabled = true;
        Vector3[] pos = new Vector3[2];
        pos[0] = player.transform.position;
        pos[1] = rb.transform.position;

        pos[0].z = 2;
        pos[1].z = 2;
        lr.SetPositions(pos);

        //If an enemy has been hit and not killed, set the position of the bullet to the enemy
        if (enCaught && caught != null)
            gameObject.transform.position = new Vector2(caught.transform.position.x, caught.transform.position.y);
    }

    void OnCollisionEnter2D(Collision2D col) {
        //If player is alive, when projectile collides, prevent force acting upon it, else destory it
        if(!player.GetComponent<PlayerController>().dead){
            collided = true;
            //If collides with an enemy, set enCaught and set caught to gameobject of enemy
            if(col.gameObject.layer == 10){
                sfx.clip = enehit;
                enCaught = true;
                caught = col.gameObject;
                gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(0,0);
            }
            else {
                sfx.clip = objhit;
            }
            sfx.Play();
            rb.bodyType = RigidbodyType2D.Static;
            Debug.Log("collided");
        }
        else {
            Destroy(lr);
            Destroy(gameObject);
        }
    }
}
