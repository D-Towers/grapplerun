﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spike : MonoBehaviour
{
    public float Limit = 20;
    // Start is called before the first frame update
    public Vector2 speed;
    bool del = false;
    public enum dir {
        up,
        down,
        left,
        right
    }
    public dir direction;
    void Start()
    {
        this.GetComponent<Rigidbody2D>().AddForce(speed);
    }

    // Update is called once per frame
    void Update() {
       //Debug.Log(direction == dir.up);
        switch (direction){
            case dir.up:
                if (transform.position.y > Limit) {
                    del = true;
                }
            break;
            case dir.down:
                if (transform.position.y < Limit) {
                    del = true;
                }
            break;
            case dir.left:
                if (transform.position.x < Limit) {
                    del = true;
                }
            break;
            case dir.right:
                if (transform.position.x > Limit) {
                    del = true;
                }
            break;
        }

        if (del){
            Destroy(gameObject);
        }
    }
}
