﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class mace : MonoBehaviour
{
    Transform Player;
    Rigidbody2D rb;
    public float yDistance;
    bool hitGround = false;
    bool falling = false;
    float time;
    AudioSource sfx;
    Vector3 start;
    // Start is called before the first frame update
    void Start() {
        Player = GameObject.Find("Player").transform;
        rb = GetComponent<Rigidbody2D>();
        start = transform.position;
        sfx = gameObject.GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update() {
        if(!hitGround && !falling && Mathf.Abs(transform.position.x - Player.position.x) < 5 ){
            if (transform.position.y - Player.position.y < yDistance){
                falling = true;
            }
        }

        if (falling) {
            rb.AddForce(new Vector2(0, -1000000 * Time.deltaTime));
        }

        if (hitGround){
            time += Time.deltaTime;
            if (time > 2) {
                rb.AddForce(new Vector2(0, 200000 * Time.deltaTime));
                if (transform.position.y >= start.y){
                    hitGround = false;
                }
            }
        }
    }


    private void OnCollisionEnter2D(Collision2D collision) {
        if (collision.gameObject.layer == 8) {
            sfx.Play();
            falling = false;
            time = 0;
            hitGround = true;
        }
    }
}
