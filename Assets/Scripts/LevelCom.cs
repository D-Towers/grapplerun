﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelCom : MonoBehaviour
{
    SpriteRenderer sr;
    bool once = false;
    bool once2 = false;
    GameObject player;
    public int levelNum;
    public float levelEndXCoord;
    string temp;
    LineRenderer lr;
    GameObject cam;
    DistanceJoint2D dj;
    float time = 0;
    float camSize;
    // Start is called before the first frame update
    void Start()
    {
        temp = PlayerPrefs.GetString("levelsComplete");
        lr = gameObject.GetComponent<LineRenderer>();
        cam = GameObject.Find("Main Camera");
        camSize = cam.GetComponent<Camera>().orthographicSize;
        sr = GetComponent<SpriteRenderer>();
        player = GameObject.Find("Player");
        gameObject.transform.position = new Vector3((cam.transform.position.x - (camSize * 2.134f)) + 2.56f , cam.transform.position.y + camSize - 2.56f, -9);
    }

    // Update is called once per frame
    void Update()
    {
        if(cam.transform.position.x > levelEndXCoord){
            cam.GetComponent<CameraScript>().enabled = false;
            if (player.transform.position.x < levelEndXCoord - 12) {
                player.transform.position = new Vector3(levelEndXCoord - 12, player.transform.position.y, player.transform.position.z);
            }

            //End level after player has wlaked off screen
            if (player.transform.position.x > levelEndXCoord + 13.5){
                time += Time.deltaTime;
                player.GetComponent<PlayerController>().enabled = false;
                //Draw a rope fromt he object to the top of the screen
                if (lr.enabled == false)
                    lr.enabled = true;
                Vector3[] pos = new Vector3[2];
                pos[0] = cam.transform.position;
                pos[1] = gameObject.transform.position;

                pos[0].z = 2;
                pos[0].y += camSize;
                pos[1].z = 2;
                lr.SetPositions(pos);

                //Enable sprite and let the object swing
                sr.enabled = true;
                sr.GetComponent<Rigidbody2D>().gravityScale = 4;
                if (dj == null) {
                    Transform trans = cam.GetComponent<Transform>();
                    dj = gameObject.AddComponent<DistanceJoint2D>();
                    dj.connectedAnchor = new Vector2(trans.position.x, trans.position.y + camSize);
                }
                if (time > 3 && !once) {
                    once = true;
                    
                    temp = temp + "" + levelNum.ToString();
                    Debug.Log(temp);
                    PlayerPrefs.SetString("levelsComplete", temp.ToString());                  
                    PlayerPrefs.SetInt("sceneTo", 0);
                    
                }
                else if (time > 3.5 && !once2){
                    once2 = true;
                    UnityEngine.SceneManagement.SceneManager.LoadScene(2);
                }
            }
        }
    }
}
