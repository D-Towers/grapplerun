﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class toScene : MonoBehaviour
{
    public UnityEngine.UI.Button btn;
    public int scene;
    bool once = false;
    // Start is called before the first frame update
    void Start()
    {
        btn.onClick.AddListener(goScene);
    }

    // Update is called once per frame
    void goScene() {
        if(!once){
            once = true;
            UnityEngine.SceneManagement.SceneManager.LoadScene(scene);
        }
    }
}
